package org.ogetman.chart;
import java.awt.BasicStroke;
import java.awt.Color;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import static javax.swing.WindowConstants.HIDE_ON_CLOSE;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.Minute;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.RefineryUtilities;
import org.ogetman.gui.FuelPanel;
import org.ogetman.xls.FuelTools;
import org.ogetman.xls.Terminal;

public class FuelChartFrame extends JFrame {

    public FuelChartFrame(final String title) {

        super(title);

        final XYDataset dataset = createDataset();
        final JFreeChart chart = createChart(dataset);
        final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(800, 570));
        chartPanel.setMouseZoomable(true, false);
        setContentPane(chartPanel);

    }

    private JFreeChart createChart(final XYDataset dataset) {

        final JFreeChart chart = ChartFactory.createTimeSeriesChart(
            "График уровня топлива",
            "Дата", 
            "Уровень топлива (л)",
            dataset,
            false,
            true,
            false
        );

        chart.setBackgroundPaint(Color.white);
        
//        final StandardLegend sl = (StandardLegend) chart.getLegend();
  //      sl.setDisplaySeriesShapes(true);

        final XYPlot plot = chart.getXYPlot();
        //plot.setOutlinePaint(null);
        plot.setBackgroundPaint(Color.white);
        plot.setDomainGridlinePaint(Color.lightGray);
        plot.setRangeGridlinePaint(Color.lightGray);
    //    plot.setAxisOffset(new Spacer(Spacer.ABSOLUTE, 5.0, 5.0, 5.0, 5.0));
        plot.setDomainCrosshairVisible(true);
        plot.setRangeCrosshairVisible(false);
        
        final XYItemRenderer renderer = plot.getRenderer();
        if (renderer instanceof StandardXYItemRenderer) {
            final StandardXYItemRenderer rr = (StandardXYItemRenderer) renderer;
            renderer.setSeriesStroke(0, new BasicStroke(2.0f));
            renderer.setSeriesStroke(1, new BasicStroke(2.0f));
           }
        
        final DateAxis axis = (DateAxis) plot.getDomainAxis();
        //axis.setDateFormatOverride(new SimpleDateFormat("hh:mma"));
        axis.setDateFormatOverride(new SimpleDateFormat("dd.MM HH:mm"));
        
        return chart;

    }

    private XYDataset createDataset() {

        final TimeSeriesCollection dataset = new TimeSeriesCollection();
        final TimeSeries s1 = new TimeSeries("Топливо");
        
        if (Terminal.MV_FUEL_COL > -1 && Terminal.DATETIME_COL > -1) {

            for (int i = 0; i < Terminal.viewTable.getRowCount(); i++) {
                try {
                    String str = (String) Terminal.viewTable.getValueAt(i, Terminal.cells.indexOf(Terminal.MV_FUEL_COL));
                    str = str.substring(0, str.indexOf(" "));
                    s1.addOrUpdate(new Minute(Terminal.dateFormat.parse((String) Terminal.viewTable.getValueAt(i, Terminal.cells.indexOf(Terminal.DATETIME_COL)))),
                            FuelTools.literFromTar(Integer.valueOf(str)));
                } catch (Exception e) {

                }
            }

        }
        dataset.addSeries(s1);
        return dataset;
    }

    public static void showChart() {

        final FuelChartFrame demo = new FuelChartFrame("График");
        demo.pack();
        demo.setDefaultCloseOperation(HIDE_ON_CLOSE);
        RefineryUtilities.centerFrameOnScreen(demo);
        demo.setVisible(true);

        FuelPanel.showChart.setText("Показать график");
        FuelPanel.showChart.setEnabled(true);
    }

}