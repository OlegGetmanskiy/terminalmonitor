package org.ogetman.chart;
import java.awt.BasicStroke;
import java.awt.Color;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import javax.swing.JFrame;
import static javax.swing.WindowConstants.HIDE_ON_CLOSE;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.Minute;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.RefineryUtilities;
import org.ogetman.gui.TempPanel;
import org.ogetman.xls.Terminal;

public class TempChartFrame extends JFrame {

    public TempChartFrame(final String title) {

        super(title);

        final XYDataset dataset = createDataset();
        final JFreeChart chart = createChart(dataset);
        final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(800, 570));
        chartPanel.setMouseZoomable(true, false);
        setContentPane(chartPanel);

    }

    private JFreeChart createChart(final XYDataset dataset) {

        final JFreeChart chart = ChartFactory.createTimeSeriesChart(
            "График температуры",
            "Дата", 
            "Температура",
            dataset,
            false,
            true,
            false
        );

        chart.setBackgroundPaint(Color.white);
        
//        final StandardLegend sl = (StandardLegend) chart.getLegend();
//        sl.setDisplaySeriesShapes(true);

        final XYPlot plot = chart.getXYPlot();
        //plot.setOutlinePaint(null);
        plot.setBackgroundPaint(Color.white);
        plot.setDomainGridlinePaint(Color.lightGray);
        plot.setRangeGridlinePaint(Color.lightGray);
    //    plot.setAxisOffset(new Spacer(Spacer.ABSOLUTE, 5.0, 5.0, 5.0, 5.0));
        plot.setDomainCrosshairVisible(true);
        plot.setRangeCrosshairVisible(false);
        
        final XYItemRenderer renderer = plot.getRenderer();
        if (renderer instanceof StandardXYItemRenderer) {
            final StandardXYItemRenderer rr = (StandardXYItemRenderer) renderer;
            renderer.setSeriesStroke(0, new BasicStroke(2.0f));
            renderer.setSeriesStroke(1, new BasicStroke(2.0f));
           }
        
        final DateAxis axis = (DateAxis) plot.getDomainAxis();
        //axis.setDateFormatOverride(new SimpleDateFormat("hh:mma"));
        axis.setDateFormatOverride(new SimpleDateFormat("dd.MM HH:mm"));
        
        return chart;

    }

    private XYDataset createDataset() {

        final TimeSeriesCollection dataset = new TimeSeriesCollection();
        final TimeSeries s1 = new TimeSeries("Температура");
        
        if(Terminal.TEMP_COL > -1 && Terminal.DATETIME_COL > -1) {
            for (int i = 0; i < Terminal.viewTable.getRowCount(); i++) {
                try {
                    double temp = Double.valueOf(((String) Terminal.viewTable.getValueAt(i, Terminal.cells.indexOf(Terminal.TEMP_COL))).replace(',', '.'));
                    if (temp != 300d) {
                        s1.addOrUpdate(new Minute(Terminal.dateFormat.parse((String) Terminal.viewTable.getValueAt(i, Terminal.cells.indexOf(Terminal.DATETIME_COL)))),
                                temp);
                    }
                } catch (Exception e) {

                }
            }
        }
        dataset.addSeries(s1);
        return dataset;
    }

    public static void showChart() {
        final TempChartFrame demo = new TempChartFrame("График");
        demo.pack();
        demo.setDefaultCloseOperation(HIDE_ON_CLOSE);
        RefineryUtilities.centerFrameOnScreen(demo);
        demo.setVisible(true);
        
        TempPanel.showChart.setText("Показать график");
        TempPanel.showChart.setEnabled(true);
    }

}