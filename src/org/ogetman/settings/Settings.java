
package org.ogetman.settings;

import java.util.prefs.Preferences;

/**
 *
 * @author OlegusGetman
 */
public class Settings {
    private final static Preferences prefs = Preferences.userRoot().node("terminal___");
    
    //Директории
    public static void SaveLastOpenedPath(String path) {
        prefs.put("lastOpenedPath", path);
    }
    
    public static String GetLastOpenedPath() {
        return prefs.get("lastOpenedPath", null);
    }
    
    public static void SaveLastExportPath(String path) {
        prefs.put("lastExportPath", path);
    }
    
    public static String GetLastExportPath() {
        return prefs.get("lastExportPath", "");
    }
    //-----------------------------------------------------
    
    //Напряжения
    //Легковые
    public static void SaveMaxBortV(float v) {
        prefs.putFloat("maxBortV",v);
    }
    
    public static float GetMaxBortV() {
        return prefs.getFloat("maxBortV", 12.5f);
    }
    
    public static void SaveMinBortV(float v) {
        prefs.putFloat("minBortV",v);
    }
    
    public static float GetMinBortV() {
        return prefs.getFloat("minBortV", 0.0f);
    }
    
    public static void SaveMaxVstrV(float v) {
        prefs.putFloat("maxVstrV",v);
    }
    
    public static float GetMaxVsrtV() {
        return prefs.getFloat("maxVstrV", 12.5f);
    }
    
    public static void SaveMinVstrV(float v) {
        prefs.putFloat("minVstrV",v);
    }
    
    public static float GetMinVsrtV() {
        return prefs.getFloat("minVstrV", 0.0f);
    }
    
    //Грузовые
    public static void SaveMaxBortVGruz(float v) {
        prefs.putFloat("maxBortVGruz",v);
    }
    
    public static float GetMaxBortVGruz() {
        return prefs.getFloat("maxBortVGruz", 30.0f);
    }
    
    public static void SaveMinBortVGruz(float v) {
        prefs.putFloat("minBortVGruz",v);
    }
    
    public static float GetMinBortVGruz() {
        return prefs.getFloat("minBortVGruz", 0.0f);
    }
    
    public static void SaveMaxVstrVGruz(float v) {
        prefs.putFloat("maxVstrVGruz",v);
    }
    
    public static float GetMaxVsrtVGruz() {
        return prefs.getFloat("maxVstrVGruz", 30.0f);
    }
    
    public static void SaveMinVstrVGruz(float v) {
        prefs.putFloat("minVstrVGruz",v);
    }
    
    public static float GetMinVsrtVGruz() {
        return prefs.getFloat("minVstrVGruz", 0.0f);
    }
    //--------------------------------------------
    
    //Скорость
    public static int GetMaxSpeed() {
        return prefs.getInt("maxSpeed", 100);
    }
    
    public static void SaveMaxSpeed(int speed) {
        prefs.putInt("maxSpeed",speed);
    }
    //--------------------------------------------
    
    //Интервалы отсутствия GSM
    public static void SaveNoGsmInterval(int v) {
        prefs.putInt("noGsmInterval",v);
    }
    
    public static int GetNoGsmInterval() {
        return prefs.getInt("noGsmInterval", 6);
    }
    //--------------------------------------------
    
    //Чекьокс "Грузовое ТС"
    public static boolean GetIsCargo() {
        return prefs.getInt("isCargoSet", 0) == 1;
    }
    
    public static void SaveIsCargo(boolean v) {
        prefs.putInt("isCargoSet", v ? 1 : 0);
    }
    //--------------------------------------------
    
    //Фильтр потери GPS
    public static void SaveNoGpsMin(int v) {
        prefs.putInt("noGpsMinMin",v);
    }
    
    public static int GetNoGpsMin() {
        return prefs.getInt("noGpsMinMin", 5);
    }
    
    public static void SaveNoGpsDistance(double v) {
        prefs.putDouble("noGpsMinDistance",v);
    }
    
    public static double GetNoGpsDistance() {
        return prefs.getDouble("noGpsMinDistance", 0.005f);
    }
    
    public static boolean GetIsGpsTimeFiltered() {
        return prefs.getInt("isGpsFiltered", 0) == 1;
    }
    
    public static void SaveIsGpsTimeFiltered(boolean v) {
        prefs.putInt("isGpsFiltered", v ? 1 : 0);
    }
    
    public static boolean GetIsGpsDistanceFiltered() {
        return prefs.getInt("isGpsDistanceFiltered", 0) == 1;
    }
    
    public static void SaveIsGpsDistanceFiltered(boolean v) {
        prefs.putInt("isGpsDistanceFiltered", v ? 1 : 0);
    }
    //--------------------------------------------
    
    //Диапазон температур
    public static void SaveMinTemp(float c) {
        prefs.putDouble("minTemp",c);
    }
    
    public static void SaveMaxTemp(float c) {
        prefs.putDouble("maxTemp",c);
    }
    
    public static float GetMinTemp() {
        return (float) prefs.getDouble("minTemp", 0);
    }
    
    public static float GetMaxTemp() {
        return (float) prefs.getDouble("maxTemp", 20);
    }
    //--------------------------------------------
    
    //Последний файл, откуда загружался XLS тарировки
    public static void SaveLastLoadedTarFile(String f) {
        prefs.put("lastTarXls", f);
    }
    
    public static String GetLastLoadedTarFile() {
        return prefs.get("lastTarXls", "");
    }
    //--------------------------------------------
    
    //Имена столбцов счетчиков 1 и 2
    public static void SaveCnt1Title(String f) {
        prefs.put("cnt1title", f);
    }
    
    public static String GetCnt1Title() {
        return prefs.get("cnt1title", "");
    }
    
    public static void SaveCnt2Title(String f) {
        prefs.put("cnt2title", f);
    }
    
    public static String GetCnt2Title() {
        return prefs.get("cnt2title", "");
    }
    //--------------------------------------------
    
    //Фильтры счетчиков 1 и 2
    public static void SaveFilterCnt1Zero(boolean v) {
        prefs.putBoolean("filterCnt1Zero",v);
    }
    
    public static boolean GetFilterCnt1Zero() {
        return prefs.getBoolean("filterCnt1Zero", false);
    }
    
    public static void SaveFilterCnt1One(boolean v) {
        prefs.putBoolean("filterCnt1One",v);
    }
    
    public static boolean GetFilterCnt1One() {
        return prefs.getBoolean("filterCnt1One", false);
    }
    
    public static void SaveFilterCnt2Zero(boolean v) {
        prefs.putBoolean("filterCnt2Zero",v);
    }
    
    public static boolean GetFilterCnt2Zero() {
        return prefs.getBoolean("filterCnt2Zero", false);
    }
    
    public static void SaveFilterCnt2One(boolean v) {
        prefs.putBoolean("filterCnt2One",v);
    }
    
    public static boolean GetFilterCnt2One() {
        return prefs.getBoolean("filterCnt2One", false);
    }
    //--------------------------------------------
    
    //Изменение топлива, которое считать сливом/заправкой
    public static float GetFuelChangeVal() {
        return (float) prefs.getDouble("fuelChangeVal", 0);
    }
    
    public static void SaveFuelChangeVal(float v) {
        prefs.putDouble("fuelChangeVal", v);
    }
    //--------------------------------------------
    
    //Фильтры топлива (сливы/заправки)
    public static boolean GetFuelFilterPlus() {
        return prefs.getBoolean("fuelFilterPlus",false);
    }
    
    public static void SaveFuelFilterPlus(boolean v) {
        prefs.putBoolean("fuelFilterPlus",v);
    }
    
    public static boolean GetFuelFilterMinus() {
        return prefs.getBoolean("fuelFilterMinus",false);
    }
    
    public static void SaveFuelFilterMinus(boolean v) {
        prefs.putBoolean("fuelFilterMinus",v);
    }
    //--------------------------------------------
    
    //Соответствие данных
    public final static String GetMappingDateTime() {
        return prefs.get("Дата и время","Дата и время");
    }
    
    public static void SaveMappingDateTime(String s) {
        prefs.put("Дата и время", s);
    }
    
    public final static String GetMappingSpeed() {
        return prefs.get("Скорость","Скорость");
    }
    
    public static void SaveMappingSpeed(String s) {
        prefs.put("Скорость", s);
    }
    
    public final static String GetMappingLatitude() {
        return prefs.get("Широта","Широта");
    }
    
    public static void SaveMappingLatitude(String s) {
        prefs.put("Широта", s);
    }
    
    public final static String GetMappingLongitude() {
        return prefs.get("Долгота","Долгота");
    }
    
    public static void SaveMappingLongitude(String s) {
        prefs.put("Долгота", s);
    }
    
    public final static String GetMappingBortV() {
        return prefs.get("Борт. (V)","Борт. (V)");
    }
    
    public static void SaveMappingBortV(String s) {
        prefs.put("Борт. (V)", s);
    }
    
    public final static String GetMappingVstrV() {
        return prefs.get("Встр. (V)","Встр. (V)");
    }
    
    public static void SaveMappingVstrV(String s) {
        prefs.put("Встр. (V)", s);
    }
    
    public final static String GetMappingDeepSleep() {
        return prefs.get("глубокий сон","глубокий сон");
    }
    
    public static void SaveMappingDeepSleep(String s) {
        prefs.put("глубокий сон", s);
    }
    
    public final static String GetMappingTemp() {
        return prefs.get("{temp 1} (C)","{temp 1} (C)");
    }
    
    public static void SaveMappingTemp(String s) {
        prefs.put("{temp 1} (C)", s);
    }
    
    public final static String GetMappingMV() {
        return prefs.get("Уровень топлива аналог (мВ)","Уровень топлива аналог (мВ)");
    }
    
    public static void SaveMappingMV(String s) {
        prefs.put("Уровень топлива аналог (мВ)", s);
    }
    //--------------------------------------------
}