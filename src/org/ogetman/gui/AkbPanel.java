
package org.ogetman.gui;

import java.awt.Rectangle;
import org.ogetman.xls.Terminal;

/**
 *
 * @author OlegusGetman
 */
public class AkbPanel extends javax.swing.JPanel {

    /**
     * Creates new form AkbPane
     */
    public AkbPanel() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane5 = new javax.swing.JScrollPane();

        highVTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null}
            },
            new String [] {
                "Дата и время", "Встр. V"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        highVTable.setToolTipText("");
        highVTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                highVTableMouseClicked(evt);
            }
        });
        jScrollPane5.setViewportView(highVTable);

        highVLabel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        highVLabel.setText("Выход встр. V за границы");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(highVLabel)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 256, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(364, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(highVLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 497, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void highVTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_highVTableMouseClicked
        if(evt.getClickCount() == 2) {
            int selectedRow = highVTable.getSelectedRow();
            //int tableRow = Terminal.findMainTableRow((String)highVTable.getValueAt(selectedRow, 0));
            int tableRow = Terminal.highVMap.get(selectedRow);
            ViewPanel.xlsContent.setRowSelectionInterval(tableRow, tableRow);
            ViewPanel.xlsContent.scrollRectToVisible(new Rectangle(ViewPanel.xlsContent.getCellRect(tableRow, 0, true)));
            MainFrame.tabbedPane.setSelectedIndex(0);
        }
    }//GEN-LAST:event_highVTableMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static final javax.swing.JLabel highVLabel = new javax.swing.JLabel();
    public static final javax.swing.JTable highVTable = new javax.swing.JTable();
    private javax.swing.JScrollPane jScrollPane5;
    // End of variables declaration//GEN-END:variables
}
