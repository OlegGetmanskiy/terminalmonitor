package org.ogetman.xls;

import java.text.DecimalFormat;

/**
 *
 * @author OlegusGetman
 */
public class DistanceTools {

    public static String getDistanceAsKm(double lat1, double lon1, double lat2, double lon2) {
        if(lat1 <= 1.0f || lat2 <= 1.0f) {
            return "";
        }
        Double d = getDistance(lat1,lon1,lat2,lon2,'K');
        if(lat1 == lat2 && lon1 == lon2) {
            return "0 км";
        }
        
        return new DecimalFormat("#.##").format(d) + " км";
    }
    
    public static double getDistance(double lat1, double lon1, double lat2, double lon2, char unit) {
        if(lat1 == 0.0f && lat2 == 0.0f) {
            return 0;
        }
        return distFrom(lon1, lat1, lon2, lat2);
    }

    private static double distFrom(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 3963.1676;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double dist = earthRadius * c;

        double meterConversion = 1.609344;

        return dist * meterConversion;
    }
}
