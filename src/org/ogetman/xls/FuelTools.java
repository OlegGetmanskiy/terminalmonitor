
package org.ogetman.xls;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JTable;
import org.ogetman.gui.SettingsPanel;

/**
 *
 * @author OlegusGetman
 */
public class FuelTools {
    public static List<Integer> mv = new ArrayList();
    public static List<Double> liters = new ArrayList();
    
    public static void addTar(int v, double liter) {
        mv.add(v);
        liters.add(liter);
    }
    
    public static void loadTarFromTable() {
        JTable table = SettingsPanel.tarTable;
        mv.clear();
        liters.clear();
        for(int i = 0; i < table.getRowCount(); i++) {
            try {
                mv.add((int)table.getValueAt(i,0));
                liters.add(Double.valueOf(table.getValueAt(i,1).toString()));
            } catch(NullPointerException e) {
                //Исключение означает, что строка в таблице тарировки пустая
                break;
            }
        }
    }
    
    public static Double literFromTar(int v) {
        if(mv.isEmpty()) return 0d;
        int i = 0;
        int currentV = mv.get(0);
        try {
            while(currentV < v) currentV = mv.get(++i);
            i--;
            double prevL = liters.get(i);
            double dL = liters.get(i+1) - prevL;
            int dV = mv.get(i+1) - mv.get(i);
            double lit = prevL + dL * (double)((double)(v-mv.get(i)) / (double)dV);
            return lit;
        } catch(IndexOutOfBoundsException e) {
           return -1d; 
        }
    }
}
