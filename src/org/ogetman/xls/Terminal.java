package org.ogetman.xls;

import java.awt.Desktop;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.OK_OPTION;
import static javax.swing.JOptionPane.YES_NO_OPTION;
import javax.swing.JProgressBar;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import jxl.Cell;
import jxl.CellView;
import jxl.DateCell;
import jxl.NumberCell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.Number;
import org.joda.time.Duration;
import org.joda.time.Interval;
import org.ogetman.gui.TempPanel;
import org.ogetman.gui.AkbPanel;
import org.ogetman.gui.BortVPanel;
import org.ogetman.gui.CntOnePanel;
import org.ogetman.gui.CntTwoPanel;
import org.ogetman.gui.FuelPanel;
import org.ogetman.gui.GsmPanel;
import org.ogetman.gui.GpsPanel;
import org.ogetman.gui.SettingsPanel;
import org.ogetman.gui.SpeedPanel;
import org.ogetman.gui.ViewPanel;
import org.ogetman.settings.Settings;

/**
 *
 * @author OlegusGetman
 */
public class Terminal {

    public static File xlsFile;
    private static Workbook wb;
    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy    HH:mm");
    public static final SimpleDateFormat periodDateFormat = new SimpleDateFormat("dd.MM.yyyy");

    public static JTable viewTable;
    private static JTable noGpsTable;
    private static JTable noGsmTable;
    private static JTable highVTable;
    private static JTable highBTable;
    private static JTable highSpeedTable;
    private static JTable tempTable;
    private static JTable fuelTable;
    private static JTable cnt1Table;
    private static JTable cnt2Table;

    public static Map<Integer, Integer> highSpeedMap;
    public static Map<Integer, Integer> highVMap;
    public static Map<Integer, Integer> highBMap;
    public static Map<Integer, Integer> noGpsMap;
    public static Map<Integer, Integer> noGsmMap;
    public static Map<Integer, Integer> tempFailMap;
    public static Map<Integer, Integer> fuelTableMap;
    public static Map<Integer, Integer> cnt1Map;
    public static Map<Integer, Integer> cnt2Map;

    public static int DATETIME_COL = -1;
    public static int SPEED_COL = -1;
    public static int LAT_COL = -1;
    public static int LONG_COL = -1;
    public static int STARTED_COL = -1;
    public static int BORTV_COL = -1;
    public static int VSTRV_COL = -1;
    public static int SLEEP_COL = -2;
    public static int TEMP_COL = -2;
    public static int MV_FUEL_COL = -2;
    public static int CNT1_COL = -2;
    public static int CNT2_COL = -2;

    public static ArrayList<Integer> cells = new ArrayList();
    public static ArrayList<String> cellTitles = new ArrayList();

    public static void setXlsFile(File file) throws IOException, BiffException {
        Terminal.xlsFile = file;
        wb = Workbook.getWorkbook(file);
    }

    public static void proceedContent(String strDateFrom, String strDateTo,
            JTable table,
            JProgressBar bar,
            JButton button,
            JTable noGpsTable,
            JTable noGsmTable,
            JTable highVTable,
            JTable highBTable,
            JTable highSpeedTable,
            JButton goButton,
            boolean isCargo,
            boolean filterGpsTime,
            boolean filterGpsDistance,
            boolean firstRun) throws IllegalArgumentException {

        Terminal.viewTable = table;
        Terminal.noGpsTable = noGpsTable;
        Terminal.noGsmTable = noGsmTable;
        Terminal.highVTable = highVTable;
        Terminal.highBTable = BortVPanel.highBTable;
        Terminal.highSpeedTable = highSpeedTable;
        Terminal.tempTable = TempPanel.tempTable;
        Terminal.fuelTable = FuelPanel.fuelTable;
        Terminal.cnt1Table = CntOnePanel.cnt1Table;
        Terminal.cnt2Table = CntTwoPanel.cnt2Table;

        FuelTools.loadTarFromTable();

        DATETIME_COL = -1;
        SPEED_COL = -1;
        LAT_COL = -1;
        LONG_COL = -1;
        STARTED_COL = -1;
        BORTV_COL = -1;
        VSTRV_COL = -1;
        SLEEP_COL = -2;
        TEMP_COL = -2;
        MV_FUEL_COL = -2;
        CNT1_COL = -2;
        CNT2_COL = -2;

        float maxBortV;
        float maxVstrV;
        float minBortV;
        float minVstrV;

        if (isCargo) {
            maxBortV = Settings.GetMaxBortVGruz();
            minBortV = Settings.GetMinBortVGruz();
        } else {
            maxBortV = Settings.GetMaxBortV();
            minBortV = Settings.GetMinBortV();
        }
        maxVstrV = Settings.GetMaxVsrtV();
        minVstrV = Settings.GetMinVsrtV();

        int maxSpeed = Settings.GetMaxSpeed();
        int noGsmInterval = Settings.GetNoGsmInterval();
        float fuelVal = Settings.GetFuelChangeVal();

        float minTemp = Settings.GetMinTemp();
        float maxTemp = Settings.GetMaxTemp();

        int minNoGpsMins = Settings.GetNoGpsMin();
        double minNoGpsDistance = Settings.GetNoGpsDistance();

        boolean filterCnt1Zero = Settings.GetFilterCnt1Zero();
        boolean filterCnt1One = Settings.GetFilterCnt1One();
        boolean filterCnt2Zero = Settings.GetFilterCnt2Zero();
        boolean filterCnt2One = Settings.GetFilterCnt2One();

        String noGpsFrom = null;
        String noGpsTo;

        Date dateNoGpsFrom = new Date();
        Date dateNoGpsTo;

        boolean stillNoGps = false;

        int noGpsCount = 0;
        int noGsmCount = 0;
        int highVCount = 0;
        int highBCount = 0;
        int highSpeedCount = 0;
        int tempFailCount = 0;
        int fuelCount = 0;
        int cnt1Count = 0;
        int cnt2Count = 0;

        double timelastLatitude;
        double timelastLongitude;

        int lastMV = -1;

        double lastLatitude = 0f;
        double lastLongitude = 0f;
        double foundLatitude;
        double foundLongitude;

        int r = 1;

        Sheet sheet = wb.getSheet(0);
        int rows = sheet.getRows();
        int cols = sheet.getColumns();

        final String cnt1Title = SettingsPanel.cnt1Title.getText();
        final String cnt2Title = SettingsPanel.cnt2Title.getText();

        boolean fuelFilterPlus = Settings.GetFuelFilterPlus();
        boolean fuelFilterMinus = Settings.GetFuelFilterMinus();

        cells.clear();
        cellTitles.clear();

        final String mappingDateTime = Settings.GetMappingDateTime();
        final String mappingSpeed = Settings.GetMappingSpeed();
        final String mappingLatitude = Settings.GetMappingLatitude();
        final String mappingLongitude = Settings.GetMappingLongitude();
        final String mappingBortV = Settings.GetMappingBortV();
        final String mappingVstrV = Settings.GetMappingVstrV();
        final String mappingDeepSleep = Settings.GetMappingDeepSleep();
        final String mappingTemp = Settings.GetMappingTemp();
        final String mappingMV = Settings.GetMappingMV();
        
        for (int i = 0; i < cols; i++) {
            Cell cell = sheet.getCell(i, 0);
            String str = cell.getContents();
            if (str.equals(mappingDateTime)) {
                DATETIME_COL = i;
                cells.add(i);
                cellTitles.add(str);
            }
            else if (str.equals(mappingSpeed)) {
                SPEED_COL = i;
                cells.add(i);
                cellTitles.add(str);
            }
            else if (str.equals(mappingLatitude)) {
                LAT_COL = i;
                cells.add(i);
                cellTitles.add(str);
            }
            else if (str.equals(mappingLongitude)) {
                LONG_COL = i;
                cells.add(i);
                cellTitles.add(str);
                /*} else if (str.contains("Зажигание")) {
                 STARTED_COL = i;
                 cells.add(i);
                 cellTitles.add(str);*/
            }
            else if (str.equals(mappingBortV)) {
                BORTV_COL = i;
                cells.add(i);
                cellTitles.add(str);
            }
            else if (str.equals(mappingVstrV)) {
                VSTRV_COL = i;
                cells.add(i);
                cellTitles.add(str);
            }
            else if (str.equals(mappingDeepSleep)) {
                SLEEP_COL = i;
                cells.add(i);
                cellTitles.add(str);
            }
            else if (str.equals(mappingTemp)) {
                TEMP_COL = i;
                cells.add(i);
                cellTitles.add(str);
            }
            else if (str.equals(mappingMV)) {
                MV_FUEL_COL = i;
                cells.add(i);
                cellTitles.add(str);
            }

            if (str.equals(cnt1Title) && !cnt1Title.trim().isEmpty()) {
                CNT1_COL = i;
                cells.add(i);
                if (!cellTitles.contains(str)) {
                    cellTitles.add(str);
                }
            }
            if (str.equals(cnt2Title) && !cnt2Title.trim().isEmpty()) {
                CNT2_COL = i;
                cells.add(i);
                if (!cellTitles.contains(str)) {
                    cellTitles.add(str);
                }
            }
        }

        /*cells.add(DATETIME_COL);
         cells.add(SPEED_COL);
         cells.add(LAT_COL);
         cells.add(LONG_COL);
         cells.add(STARTED_COL);
         cells.add(BORTV_COL);
         cells.add(VSTRV_COL);
         cells.add(SLEEP_COL);
         cells.add(TEMP_COL);
         cells.add(MV_FUEL_COL);*/
        for (int i : cells) {
            if (i == -1) {
                JOptionPane.showMessageDialog(null, "В XLS-файле не удалось найти некоторые столбцы,\n из-за чего "
                        + "анализ может быть неточным или неполным.\n"
                        + "В таблице должны как минимум находиться столбцы:\n"
                        + "- Дата и время\n"
                        + "- Скорость\n"
                        + "- Широта\n"
                        + "- Долгота\n"
                        + "Также расчет проводится по столбцами:\n"
                        + "- Борт. (V)\n"
                        + "- Встр. (V)\n"
                        + "- глубокий сон\n");
                goButton.setEnabled(true);
                break;
            }
        }

        if (firstRun) {
            strDateFrom = "";
            strDateTo = "";
        }

        if (!strDateFrom.trim().isEmpty() && DATETIME_COL > 0) {
            try {
                Date dateFrom = periodDateFormat.parse(strDateFrom);
                Calendar c = Calendar.getInstance();
                c.setTime(dateFrom);
                c.add(Calendar.HOUR, 4);
                dateFrom = c.getTime();

                for (int i = 1; i < rows; i++) {
                    try {
                        DateCell cell = (DateCell) sheet.getCell(DATETIME_COL, i);
                        if (cell.getDate().after(dateFrom)) {
                            r = cell.getRow();
                            break;
                        }
                    } catch (ClassCastException e) {
                        r = rows - 1;
                    }
                }
            } catch (ParseException e) {
                throw new IllegalArgumentException();
            }
        }
        if (!strDateTo.trim().isEmpty() && DATETIME_COL > 0) {
            try {
                Date dateTo = periodDateFormat.parse(strDateTo);
                Calendar c = Calendar.getInstance();
                c.setTime(dateTo);
                c.add(Calendar.DATE, 1);
                c.add(Calendar.HOUR, 4);

                dateTo = c.getTime();
                DateCell cell = (DateCell) sheet.getCell(DATETIME_COL, 1);
                try {
                    while (cell.getDate().before(dateTo) && cell.getRow() < rows - 1) {
                        cell = (DateCell) sheet.getCell(DATETIME_COL, cell.getRow() + 1);
                    }
                    rows = cell.getRow() + 1;
                } catch (ClassCastException e) {

                }
            } catch (ParseException e) {
                throw new IllegalArgumentException();
            }
        }

        bar.setValue(0);
        bar.setString("Парсинг XLS...");
        bar.setMaximum(rows - 1 - r);
        DefaultTableModel dtm = (DefaultTableModel) table.getModel();
        dtm.setRowCount(rows - 1 - r > -1 ? rows - 1 - r : 0);
        dtm.setColumnCount(cells.size());
        dtm.setColumnIdentifiers(cellTitles.toArray());
        table.setModel(dtm);

        DefaultTableModel noGpsModel = (DefaultTableModel) noGpsTable.getModel();
        noGpsModel.setRowCount(rows + 5 - r);

        DefaultTableModel highVModel = (DefaultTableModel) highVTable.getModel();
        highVModel.setRowCount(rows + 5 - r);

        DefaultTableModel highBModel = (DefaultTableModel) highBTable.getModel();
        highBModel.setRowCount(rows + 5 - r);

        DefaultTableModel highSpeedModel = (DefaultTableModel) highSpeedTable.getModel();
        highSpeedModel.setRowCount(rows + 5 - r);

        DefaultTableModel noGsmModel = (DefaultTableModel) noGsmTable.getModel();
        noGsmModel.setRowCount(rows + 5 - r);

        DefaultTableModel tempModel = (DefaultTableModel) tempTable.getModel();
        tempModel.setRowCount(rows + 5 - r);

        DefaultTableModel fuelModel = (DefaultTableModel) fuelTable.getModel();
        fuelModel.setRowCount(rows + 5 - r);

        DefaultTableModel cnt1Model = (DefaultTableModel) cnt1Table.getModel();
        cnt1Model.setRowCount(rows + 5 - r);

        DefaultTableModel cnt2Model = (DefaultTableModel) cnt2Table.getModel();
        cnt2Model.setRowCount(rows + 5 - r);

        int rowCount = 0;

        highSpeedMap = new HashMap();
        highVMap = new HashMap();
        highBMap = new HashMap();
        noGpsMap = new HashMap();
        noGsmMap = new HashMap();
        tempFailMap = new HashMap();
        fuelTableMap = new HashMap();
        cnt1Map = new HashMap();
        cnt2Map = new HashMap();

        String cnt1CurrentData = "-";
        String dateLastCnt1 = null;
        String cnt2CurrentData = "-";
        String dateLastCnt2 = null;

        boolean fuelFirstRowSet = false;
        int fuelFirstRow = 0;
        double fuelDiffCollect = 0;
        String fuelFirstDatetime = "";
        double fuelFirstLat = 0;
        double fuelFirstLon = 0;

        int curRow = 0;
        //Обрабатываем все строки
        for (; r < rows - 1; r++) {
            curRow++;
            for (int c : cells) {
                try {
                    if (c < 0) {
                        continue;
                    }
                    if (c == LAT_COL || c == LONG_COL) {
                        NumberCell cell = (NumberCell) sheet.getCell(c, r);
                        double v = (double) cell.getValue();
                        table.setValueAt(v, rowCount, cells.indexOf(c));
                        continue;
                    }

                    String value = sheet.getCell(c, r).getContents();
                    table.setValueAt(value, rowCount, cells.indexOf(c));
                    bar.setValue(r);

                    //Даты обрабатывать по-особому
                    if (c == DATETIME_COL) {
                        DateCell cell = (DateCell) sheet.getCell(c, r);
                        Date date = cell.getDate();
                        table.setValueAt(formatDate(date), rowCount, cells.indexOf(c));
                        if (firstRun) {
                            try {
                                if (r == 1) {
                                    ViewPanel.datePickerFrom.setDate(date);
                                }

                                if (r == rows - 2) {
                                    ViewPanel.datePickerTo.setDate(date);
                                }
                            } catch (PropertyVetoException e) {
                                System.out.println(e);
                            }
                        }

                        if (r > 1) {
                            DateCell prevCell = (DateCell) sheet.getCell(c, r - 1);
                            Date prevDate = prevCell.getDate();
                            if ((date.getTime() - prevDate.getTime()) / (60 * 1000) > noGsmInterval) { //Разница в минутах

                                boolean isCurrentSleep = false;
                                boolean isPrevSleep = false;
                                if (SLEEP_COL > 0) {
                                    isCurrentSleep = sheet.getCell(SLEEP_COL, r).getContents().contains("1");
                                    isPrevSleep = sheet.getCell(SLEEP_COL, r - 1).getContents().contains("1");
                                }

                                if (!isCurrentSleep && !isPrevSleep) {
                                    noGsmTable.setValueAt(formatDate(prevDate), noGsmCount, 0);
                                    noGsmTable.setValueAt(formatDate(date), noGsmCount, 1);
                                    noGsmTable.setValueAt(dateDiff(prevDate, date), noGsmCount, 6);
                                    timelastLatitude = ((NumberCell) sheet.getCell(LAT_COL, r - 1)).getValue();
                                    timelastLongitude = ((NumberCell) sheet.getCell(LONG_COL, r - 1)).getValue();
                                    noGsmTable.setValueAt(timelastLatitude, noGsmCount, 2);
                                    noGsmTable.setValueAt(timelastLongitude, noGsmCount, 3);
                                    foundLatitude = ((NumberCell) sheet.getCell(3, r)).getValue();
                                    foundLongitude = ((NumberCell) sheet.getCell(4, r)).getValue();
                                    noGsmTable.setValueAt(foundLatitude, noGsmCount, 4);
                                    noGsmTable.setValueAt(foundLongitude, noGsmCount, 5);
                                    noGsmTable.setValueAt(DistanceTools.getDistanceAsKm(timelastLatitude, timelastLongitude, foundLatitude, foundLongitude), noGsmCount, 7);
                                    noGsmMap.put(noGsmCount, curRow - 1);
                                    noGsmCount++;
                                }
                            }
                        }
                    }

                    //Скорость "N/A" означает отсутствие GPS сигнала
                    if (c == SPEED_COL && value.contains("N/A")) {
                        if (!stillNoGps) {
                            if (r > 1) {
                                lastLatitude = ((NumberCell) sheet.getCell(LAT_COL, r - 1)).getValue();
                                lastLongitude = ((NumberCell) sheet.getCell(LONG_COL, r - 1)).getValue();
                                noGpsMap.put(noGpsCount, curRow - 1);
                            } else {
                                lastLatitude = 0f;
                                lastLongitude = 0f;
                            }
                            noGpsFrom = formatDate(((DateCell) sheet.getCell(DATETIME_COL, r)).getDate());
                            dateNoGpsFrom = ((DateCell) sheet.getCell(DATETIME_COL, r)).getDate();
                            //noGpsTable.setValueAt(noGpsFrom, noGpsCount, 0);
                            noGpsTable.setValueAt(lastLatitude, noGpsCount, 2);
                            noGpsTable.setValueAt(lastLongitude, noGpsCount, 3);
                            stillNoGps = true;
                        } else {
                            if (r == rows - 2) {
                                noGpsTo = formatDate(((DateCell) sheet.getCell(DATETIME_COL, r)).getDate());
                                dateNoGpsTo = ((DateCell) sheet.getCell(DATETIME_COL, r - 1)).getDate();

                                noGpsTable.setValueAt(noGpsTo, noGpsCount, 1);

                                foundLatitude = 0.0;
                                foundLongitude = 0.0;

                                noGpsTable.setValueAt(noGpsFrom, noGpsCount, 0);
                                noGpsTable.setValueAt(noGpsTo, noGpsCount, 1);
                            //noGpsTable.setValueAt(lastLatitude, noGpsCount, 2);
                                //noGpsTable.setValueAt(lastLongitude, noGpsCount, 3);
                                noGpsTable.setValueAt(foundLatitude, noGpsCount, 4);
                                noGpsTable.setValueAt(foundLongitude, noGpsCount, 5);
                                noGpsTable.setValueAt(dateDiff(dateNoGpsFrom, dateNoGpsTo), noGpsCount, 6);
                                noGpsMap.put(noGpsCount, curRow - 1);
                                noGpsCount++;
                            }
                        }
                    } else if (c == SPEED_COL && !value.contains("N/A")) {
                        if (stillNoGps) {
                            stillNoGps = false;
                            noGpsTo = formatDate(((DateCell) sheet.getCell(DATETIME_COL, r)).getDate());
                            dateNoGpsTo = ((DateCell) sheet.getCell(DATETIME_COL, r)).getDate();

                            foundLatitude = ((NumberCell) sheet.getCell(LAT_COL, r)).getValue();
                            foundLongitude = ((NumberCell) sheet.getCell(LONG_COL, r)).getValue();

                            double distance = DistanceTools.getDistance(lastLatitude, lastLongitude, foundLatitude, foundLongitude, 'K');
                            long timeDif = (dateNoGpsTo.getTime() - dateNoGpsFrom.getTime()) / (60 * 1000);

                            boolean filteredByTime = filterGpsTime ? (timeDif <= minNoGpsMins) : false;
                            boolean filteredByDistance = filterGpsDistance ? (distance <= minNoGpsDistance) : false;

                            if (!filteredByTime && !filteredByDistance) {
                                noGpsTable.setValueAt(noGpsFrom, noGpsCount, 0);
                                noGpsTable.setValueAt(noGpsTo, noGpsCount, 1);
                                noGpsTable.setValueAt(lastLatitude, noGpsCount, 2);
                                noGpsTable.setValueAt(lastLongitude, noGpsCount, 3);

                                noGpsTable.setValueAt(foundLatitude, noGpsCount, 4);
                                noGpsTable.setValueAt(foundLongitude, noGpsCount, 5);
                                noGpsTable.setValueAt(dateDiff(dateNoGpsFrom, dateNoGpsTo), noGpsCount, 6);

                                double gotLastLat = (double) noGpsTable.getValueAt(noGpsCount, 2);
                                double gotLastLong = (double) noGpsTable.getValueAt(noGpsCount, 3);

                                noGpsTable.setValueAt(DistanceTools.getDistanceAsKm(gotLastLat, gotLastLong, foundLatitude, foundLongitude), noGpsCount, 7);
                                //noGpsSet.put(noGpsCount, r-1);
                                noGpsCount++;
                            }
                        }

                        NumberCell cell;
                        int speed;
                        try {
                            cell = (NumberCell) sheet.getCell(c, r);
                            speed = (int) cell.getValue();
                        } catch (ClassCastException e) {
                            speed = 0;
                        }

                        if (speed > maxSpeed) {
                            highSpeedTable.setValueAt(formatDate(((DateCell) (sheet.getCell(DATETIME_COL, r))).getDate()), highSpeedCount, 0);
                            highSpeedTable.setValueAt(speed, highSpeedCount, 1);
                            highSpeedMap.put(highSpeedCount, curRow - 1);
                            highSpeedCount++;
                        }
                    }
                //-----------------------------------------------

                    //Считаем напряжения
                    if (c == BORTV_COL) {
                        if (sheet.getCell(BORTV_COL, r) instanceof NumberCell) {
                            NumberCell bortCell = (NumberCell) sheet.getCell(BORTV_COL, r);
                            float b = (float) bortCell.getValue();
                            if (b > maxBortV || b < minBortV) {
                                String datetime;
                                if (DATETIME_COL > 0) {
                                    datetime = formatDate(((DateCell) sheet.getCell(DATETIME_COL, r)).getDate());
                                } else {
                                    datetime = "";
                                }

                                String bortB = sheet.getCell(BORTV_COL, r).getContents();
                                highBTable.setValueAt(datetime, highBCount, 0);
                                highBTable.setValueAt(bortB, highBCount, 1);
                                highBMap.put(highBCount, curRow - 1);
                                highBCount++;
                            }
                        }

                        if (sheet.getCell(VSTRV_COL, r) instanceof NumberCell) {
                            NumberCell vstrCell = (NumberCell) sheet.getCell(VSTRV_COL, r);
                            float v = (float) vstrCell.getValue();
                            if (v > maxVstrV || v < minVstrV) {
                                String datetime;
                                if (DATETIME_COL > 0) {
                                    datetime = formatDate(((DateCell) sheet.getCell(DATETIME_COL, r)).getDate());
                                } else {
                                    datetime = "";
                                }

                                String bortV = sheet.getCell(VSTRV_COL, r).getContents();
                                highVTable.setValueAt(datetime, highVCount, 0);
                                highVTable.setValueAt(bortV, highVCount, 1);
                                highVMap.put(highVCount, curRow - 1);
                                highVCount++;
                            }
                        }
                    }

                    //Считаем температуру
                    if (c == TEMP_COL) {
                        Cell tempCell = sheet.getCell(TEMP_COL, r);
                        float t;
                        try {
                            t = Float.valueOf(tempCell.getContents());
                        } catch (NumberFormatException e) {
                            t = 0f;
                        }

                        if ((t < minTemp || t > maxTemp) && t != 300) {
                            String datetime;
                            if (DATETIME_COL > 0) {
                                datetime = formatDate(((DateCell) sheet.getCell(DATETIME_COL, r)).getDate());
                            } else {
                                datetime = "";
                            }
                            String temp = sheet.getCell(TEMP_COL, r).getContents();
                            tempTable.setValueAt(datetime, tempFailCount, 0);
                            tempTable.setValueAt(temp, tempFailCount, 1);
                            tempFailMap.put(tempFailCount, curRow - 1);
                            tempFailCount++;
                        }
                    }

                    //Считаем mV топливо
                    if (c == MV_FUEL_COL) {
                        NumberCell tempCell = (NumberCell) sheet.getCell(MV_FUEL_COL, r);
                        table.setValueAt(tempCell.getContents() + " = " + String.format("%.2f L", FuelTools.literFromTar(Integer.valueOf(tempCell.getContents()))), rowCount, cells.indexOf(c));
                        //table.setValueAt(String.format("%.2f", FuelTools.literFromTar(Integer.valueOf(tempCell.getContents()))), rowCount, cells.indexOf(c));

                        int mv = (int) tempCell.getValue();
                        double diff = FuelTools.literFromTar(mv) - FuelTools.literFromTar(lastMV);
                        String datetime;
                        if (DATETIME_COL > 0) {
                            datetime = formatDate(((DateCell) sheet.getCell(DATETIME_COL, r)).getDate());
                        } else {
                            datetime = "";
                        }
                        double lat;
                        double lon;
                        try {
                            lat = ((NumberCell) sheet.getCell(LAT_COL, r)).getValue();
                            lon = ((NumberCell) sheet.getCell(LONG_COL, r)).getValue();
                            //lat = String.valueOf((sheet.getCell(LAT_COL, r - 1)).getContents());
                            //lon = String.valueOf((sheet.getCell(LONG_COL, r - 1)).getContents());
                        } catch (NumberFormatException | ClassCastException e) {
                            lat = 0;
                            lon = 0;
                        }

                        int speed;
                        try {
                            if (SPEED_COL >= 0) {
                                NumberCell cell = (NumberCell) sheet.getCell(SPEED_COL, r);
                                speed = (int) cell.getValue();
                            } else {
                                speed = 0;
                            }
                        } catch (ClassCastException e) {
                            speed = 0;
                        }
                        
                        if ((/*((Math.abs(diff) > fuelVal) || Math.signum(diff) == 1) &&*/ lastMV > -1)) {
                            //if ((diff < 0 && onlyMinus) || (diff > 0 && onlyPlus) || everything) {
                            Date curDate = ((DateCell) sheet.getCell(DATETIME_COL, r)).getDate();
                            Date prevDate = curDate;
                            double nextDiff = 0;
                            if (r > 0 && r < rows - 2) {
                                prevDate = ((DateCell) sheet.getCell(DATETIME_COL, r - 1)).getDate();
                                int nextMV = (int) ((NumberCell) sheet.getCell(MV_FUEL_COL, r + 1)).getValue();
                                nextDiff = FuelTools.literFromTar(nextMV) - FuelTools.literFromTar(mv);
                            }
                            long timeDiff = (curDate.getTime() - prevDate.getTime()) / (60 * 1000);
                            fuelDiffCollect += diff;
                            if (/*(timeDiff > 10) || */(Math.signum(diff) != Math.signum(nextDiff))) {
                                int speedRow = fuelFirstRow;
                                /*if (speed != 0) {
                                    //принудительный поиск координат со скоростью 0
                                    while (speed != 0 && speedRow != rows - 1) {
                                        speedRow++;
                                        try {
                                            if (SPEED_COL >= 0) {
                                                NumberCell cell = (NumberCell) sheet.getCell(SPEED_COL, speedRow);
                                                speed = (int) cell.getValue();
                                            } else {
                                                speed = 0;
                                            }
                                        } catch (ClassCastException e) {
                                            speed = 0;
                                        }
                                        try {
                                            fuelFirstLat = String.valueOf(((NumberCell) sheet.getCell(LAT_COL, speedRow)).getValue());
                                            fuelFirstLon = String.valueOf(((NumberCell) sheet.getCell(LONG_COL, speedRow)).getValue());
                                        } catch (NumberFormatException | ClassCastException e) {
                                            fuelFirstLat = "0";
                                            fuelFirstLon = "0";
                                        }
                                    }
                                }*/
                                fuelTable.setValueAt(fuelFirstDatetime, fuelCount, 0);
                                fuelTable.setValueAt(fuelFirstLat, fuelCount, 1);
                                fuelTable.setValueAt(fuelFirstLon, fuelCount, 2);
                                fuelTable.setValueAt(fuelDiffCollect, fuelCount, 3);
                                fuelTableMap.put(fuelCount, fuelFirstRow);
                                fuelFirstRowSet = false;
                                fuelCount++;
                                fuelDiffCollect = 0;
                            } else {
                                //fuelDiffCollect += diff;
                            }
                            //}
                            //if(timeDiff > 10) {
                            if(speed <= 5 && !fuelFirstRowSet) {
                                fuelFirstDatetime = datetime;
                                fuelFirstLat = lat;
                                fuelFirstLon = lon;
                                fuelFirstRow = curRow;
                                fuelFirstRowSet = true;
                            }
                            //}
                        }
                        if (mv > 0) {
                            lastMV = mv;
                        }
                    }

                    //Считаем счетчик 1
                    if (c == CNT1_COL) {
                        String state = sheet.getCell(CNT1_COL, r).getContents();
                        boolean onlyOne = filterCnt1One && !filterCnt1Zero;
                        boolean onlyZero = filterCnt1Zero && !filterCnt1One;
                        boolean both = filterCnt1One && filterCnt1Zero;
                        boolean everything = !filterCnt1One && !filterCnt1Zero;

                        boolean filtered = (onlyOne && state.equals(""))
                                || (onlyZero && state.equals("1")
                                || (both && (state.equals("") || state.equals("1"))) /*|| everything*/);

                        if (dateLastCnt1 == null) {
                            dateLastCnt1 = formatDate(((DateCell) sheet.getCell(DATETIME_COL, r)).getDate());
                            if (filtered) {
                                cnt1Table.setValueAt(dateLastCnt1, cnt1Count, 0);
                            }
                            cnt1CurrentData = sheet.getCell(CNT1_COL, r).getContents();
                        }
                        if ((curRow == rows - 2) && ((cnt1CurrentData.equals("") && filterCnt1Zero) || (cnt1CurrentData.equals("1") && filterCnt1One))) {
                            cnt1Table.setValueAt(dateLastCnt1, cnt1Count, 0);
                            cnt1Table.setValueAt(formatDate(((DateCell) sheet.getCell(DATETIME_COL, r)).getDate()), cnt1Count, 1);
                            cnt1Table.setValueAt(cnt1CurrentData, cnt1Count, 2);
                            cnt1Map.put(cnt1Count, curRow - 1);
                            cnt1Count++;
                        }
                        if (!state.equals(cnt1CurrentData)) {
                            if (filtered) {
                                cnt1Table.setValueAt(dateLastCnt1, cnt1Count, 0);
                                cnt1Table.setValueAt(formatDate(((DateCell) sheet.getCell(DATETIME_COL, r)).getDate()), cnt1Count, 1);
                                cnt1Table.setValueAt(cnt1CurrentData, cnt1Count, 2);
                                cnt1Map.put(cnt1Count, curRow - 1);
                                cnt1Count++;
                            }
                            cnt1CurrentData = state;
                            dateLastCnt1 = formatDate(((DateCell) sheet.getCell(DATETIME_COL, r)).getDate());
                        }
                    }

                    //Считаем счетчик 2
                    if (c == CNT2_COL) {
                        String state = sheet.getCell(CNT2_COL, r).getContents();
                        boolean onlyOne = filterCnt2One && !filterCnt2Zero;
                        boolean onlyZero = filterCnt2Zero && !filterCnt2One;
                        boolean both = filterCnt2One && filterCnt2Zero;
                        boolean everything = !filterCnt2One && !filterCnt2Zero;

                        boolean filtered = (onlyOne && state.equals(""))
                                || (onlyZero && state.equals("1")
                                || (both && (state.equals("") || state.equals("1"))) /*|| everything*/);

                        if (dateLastCnt2 == null) {
                            dateLastCnt2 = formatDate(((DateCell) sheet.getCell(DATETIME_COL, r)).getDate());
                            if (filtered) {
                                cnt2Table.setValueAt(dateLastCnt2, cnt1Count, 0);
                            }
                            cnt2CurrentData = sheet.getCell(CNT2_COL, r).getContents();
                        }
                        if ((curRow == rows - 2) && ((cnt2CurrentData.equals("") && filterCnt2Zero) || (cnt2CurrentData.equals("1") && filterCnt2One))) {
                            cnt2Table.setValueAt(dateLastCnt2, cnt2Count, 0);
                            cnt2Table.setValueAt(formatDate(((DateCell) sheet.getCell(DATETIME_COL, r)).getDate()), cnt2Count, 1);
                            cnt2Table.setValueAt(cnt2CurrentData, cnt2Count, 2);
                            cnt2Map.put(cnt2Count, curRow - 1);
                            cnt2Count++;
                        }
                        if (!state.equals(cnt2CurrentData)) {
                            if (filtered) {
                                cnt2Table.setValueAt(dateLastCnt2, cnt2Count, 0);
                                cnt2Table.setValueAt(formatDate(((DateCell) sheet.getCell(DATETIME_COL, r)).getDate()), cnt2Count, 1);
                                cnt2Table.setValueAt(cnt2CurrentData, cnt2Count, 2);
                                cnt2Map.put(cnt2Count, curRow - 1);
                                cnt2Count++;
                            }
                            cnt2CurrentData = state;
                            dateLastCnt2 = formatDate(((DateCell) sheet.getCell(DATETIME_COL, r)).getDate());
                        }
                    }
                } catch (ClassCastException | NumberFormatException e) {
                    //System.out.println(e);
                }
            }
            rowCount++;
        }
        noGpsModel.setRowCount(noGpsCount);
        noGsmModel.setRowCount(noGsmCount);
        highVModel.setRowCount(highVCount);
        highBModel.setRowCount(highBCount);
        highSpeedModel.setRowCount(highSpeedCount);
        tempModel.setRowCount(tempFailCount);
        fuelModel.setRowCount(fuelCount);
        cnt1Model.setRowCount(cnt1Count);
        cnt2Model.setRowCount(cnt2Count);

        correctCntTables();

        bar.setString("Готово");

        //Установка надписей
        GsmPanel.noSignalLabel.setText("<html><font color=\"red\">Потери GSM (более " + noGsmInterval + " мин). Количество: " + noGsmTable.getRowCount());
        GpsPanel.noGpsLabel.setText("<html><font color=\"red\">Потери GPS (" + noGpsTable.getRowCount() + ")");
        BortVPanel.highBLabel.setText("<html><font color=\"red\">Выход борт. V за границы " + String.format("(%s - %s).", minBortV, maxBortV)
                + " Количество: " + highBTable.getRowCount() + "</font>");
        AkbPanel.highVLabel.setText("<html><font color=\"red\">Выход встр.V за границы " + String.format("(%s - %s).", minVstrV, maxVstrV)
                + " Количество: " + highVTable.getRowCount());
        TempPanel.tempLabel.setText("<html><font color=\"red\">Нарушения температуры (" + Settings.GetMinTemp() + " C - " + Settings.GetMaxTemp() + " C)."
                + "<br>Количество: " + tempTable.getRowCount() + "</font>");
        SpeedPanel.highSpeedLabel.setText("<html><font color=\"red\">Превышение скоростного режима " + Settings.GetMaxSpeed() + " км/час."
                + " Количество: " + highSpeedTable.getRowCount() + "</font>");
        //FuelPanel.fuelLabel.setText("<html><font color=\"red\">Изменения уровня топлива (>" + Settings.GetFuelChangeVal() + " л)."
        //+ " Количество: " + fuelTable.getRowCount() + "</font>");
        CntOnePanel.cnt1Label.setText("<html><font color=\"red\">Изменение состояния (" + cnt1Table.getRowCount() + ")" + (cnt1Title.trim().isEmpty() ? "" : " [" + cnt1Title + "]") + "</font>");
        CntTwoPanel.cnt2Label.setText("<html><font color=\"red\">Изменение состояния (" + cnt2Table.getRowCount() + ")" + (cnt2Title.trim().isEmpty() ? "" : " [" + cnt2Title + "]") + "</font>");
        //-------------------

        //correctFuelTable();
        ViewPanel.progressBar.setIndeterminate(true);
        filterFuelTable(fuelFilterPlus, fuelFilterMinus);
        button.setEnabled(true);
        ViewPanel.progressBar.setIndeterminate(false);
    }

    private static String formatDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.HOUR_OF_DAY, -4);
        return dateFormat.format(cal.getTime());
    }

    private static String dateDiff(Date from, Date to) {
        Interval interval = new Interval(from.getTime(), to.getTime());
        Duration d = interval.toDuration();
        int hours = (int) d.getStandardHours();
        int mins = (int) d.getStandardMinutes();
        while (mins > 59) {
            mins -= 60;
        }
        return String.format("%d:%02d", hours, mins);
    }

    public static void openMapsLink(String latitude, String longitude, String label) {
        if (latitude.startsWith("0.0") || longitude.startsWith("0.0")) {
            JOptionPane.showMessageDialog(null, "Координаты отсутствуют", "Ошибка", OK_OPTION);
            return;
        }
        try {
            String link = "http://maps.google.com/"
                    + "?q=" + latitude + "," + longitude + "+(" + URLEncoder.encode(label, "UTF-8") + ")"
                    + "&z=13"
                    + "&t=h"
                    + "&iwloc=A"
                    + "&hl=ru";

            Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
            if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
                try {
                    desktop.browse(new URI(link));
                } catch (IOException | URISyntaxException e) {
                    System.out.println(e);
                }
            }
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Terminal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void exportToFile(String file) {
        if (xlsFile == null) {
            JOptionPane.showMessageDialog(null, "Вы еще не загрузили XLS-файл в программу", "Ошибка", OK_OPTION);
            return;
        }
        try {
            File f = new File(file);
            WritableWorkbook wbook = Workbook.createWorkbook(f);
            fillDataToXls(wbook, noGpsTable, f, "Потери сигнала GPS", 0);
            fillDataToXls(wbook, noGsmTable, f, "Потери сигнала GSM", 1);
            fillDataToXls(wbook, highVTable, f, "Превышения встр.V", 2);
            fillDataToXls(wbook, highBTable, f, "Превышения борт.V", 3);
            fillDataToXls(wbook, highSpeedTable, f, "Превышения скоростного режима", 4);
            fillDataToXls(wbook, tempTable, f, "Нарушения температур", 5);
            fillDataToXls(wbook, fuelTable, f, "Топливо", 6);
            fillDataToXls(wbook, cnt1Table, f, "Датчик 1", 7);
            fillDataToXls(wbook, cnt2Table, f, "Датчик 2", 8);

            wbook.write();
            wbook.close();
            if (JOptionPane.showConfirmDialog(null, "Хотите открыть файл?", "Успешно экспортировано", YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                Desktop.getDesktop().open(f);
            }
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Вы еще не загрузили XLS файл в программу,\nили указанный файл невозможно создать", "Ошибка", OK_OPTION);
        } catch (WriteException ex) {
            JOptionPane.showMessageDialog(null, "Ошибка при записи в файл. Не используется ли он другими программами?", "Ошибка", OK_OPTION);
        }
    }

    private static boolean isNumeric(String v) {
        try {
            double x = Double.valueOf(v);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    public static void fillDataToXls(WritableWorkbook wbook, JTable table, File file, String sheet, int sheetNum) {
        WritableCellFormat wcf = new WritableCellFormat();
        try {
            wcf.setAlignment(Alignment.RIGHT);
            WritableSheet sheet1 = wbook.createSheet(sheet, sheetNum);
            TableModel model = table.getModel();

            for (int i = 0; i < model.getColumnCount(); i++) {

                CellView view = sheet1.getColumnView(i);
                view.setSize(6000);
                sheet1.setColumnView(i, view);

                Label column = new Label(i, 0, model.getColumnName(i));
                sheet1.addCell(column);
            }
            for (int i = 0; i < model.getRowCount(); i++) {
                for (int j = 0; j < model.getColumnCount(); j++) {
                    Label row = new Label(j, i + 1, model.getValueAt(i, j).toString());
                    row.setCellFormat(wcf);
                    if (isNumeric(row.getString())) {
                        sheet1.addCell(new Number(j, i + 1, Double.valueOf(model.getValueAt(i, j).toString())));
                    } else {
                        sheet1.addCell(row);
                    }
                }
            }
        } catch (WriteException ex) {
            System.out.println(ex);
        }
    }

    public static int findMainTableRow(String date) {
        try {
            int row = 0;
            TableModel model = viewTable.getModel();
            for (; row < model.getRowCount(); row++) {
                String viewValue = (String) model.getValueAt(row, 0);
                if (viewValue.contains(date)) {
                    return row;
                }
            }
            return 0;
        } catch (NullPointerException e) {
            //Ничего не делать, исключение означает, что таблица viewTable пуста
            return 0;
        }
    }

    public static void correctFuelTable() {
        //if(true)return;
        DefaultTableModel fuelModel = (DefaultTableModel) fuelTable.getModel();
        for (int i = 0; i < fuelModel.getRowCount(); i++) {
            try {
                Date currentDate = dateFormat.parse((String) fuelTable.getValueAt(i, 0));
                double currentFuel = (double) fuelTable.getValueAt(i, 3);
                if (i + 1 < fuelModel.getRowCount() - 1) {
                    Date nextDate = dateFormat.parse((String) fuelTable.getValueAt(i + 1, 0));
                    double nextFuel = (double) fuelTable.getValueAt(i + 1, 3);

                    long dateDiff = (nextDate.getTime() - currentDate.getTime()) / (1000 * 60);

                    if (dateDiff <= 8) {
                        //System.out.println("---");
                        //System.out.println(fuelTableMap);
                        currentFuel += nextFuel;
                        fuelTable.setValueAt(currentFuel, i, 3);
                        fuelModel.removeRow(i + 1);
                        //fuelTableMap.remove(i+1);
                        try {
                            for (Integer key : fuelTableMap.keySet()) {
                                if (key > i) {
                                    fuelTableMap.put(key, fuelTableMap.get(key + 1));
                                }
                            }
                            //System.out.println(fuelTableMap);
                        } catch (ConcurrentModificationException e) {
                            System.out.println("!");
                        }
                    }
                }
            } catch (ParseException ex) {
                Logger.getLogger(Terminal.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static void filterFuelTable(boolean showPlus, boolean showMinus) {
        //if(true)return;
        //System.out.printf("==== [ showPlus = %b, showMinus = %b\n",showPlus,showMinus);
        double fuelVal = Settings.GetFuelChangeVal();
        DefaultTableModel fuelModel = (DefaultTableModel) fuelTable.getModel();
        if (!showPlus && !showMinus) {
            fuelModel.getDataVector().removeAllElements();
            //System.out.println("removing all elements");
            return;
        }
        
        int i = 1;
        while (i < fuelModel.getRowCount()) {
            double prevFuel = 0;
            try {
                prevFuel = (double) fuelTable.getValueAt(i-1, 3);
            } catch(Exception e) {
                prevFuel = 0;
            }
            double currentFuel = (double) fuelTable.getValueAt(i, 3);
            double fuelD = currentFuel + prevFuel;
            //System.out.printf("seeing %f, %f, %f\n",prevFuel,currentFuel,fuelD);
            if (Math.abs(fuelD) < 3) {
                fuelModel.removeRow(i);
                try {
                    for (Integer key : fuelTableMap.keySet()) {
                        if (key >= i) {
                            fuelTableMap.put(key, fuelTableMap.get(key + 1));
                        }
                    }
                } catch (ConcurrentModificationException e) {
                    System.out.println("!");
                }
                fuelModel.removeRow(i-1);
                try {
                    for (Integer key : fuelTableMap.keySet()) {
                        if (key >= i-1) {
                            fuelTableMap.put(key, fuelTableMap.get(key+1));
                        }
                    }
                } catch (ConcurrentModificationException e) {
                    System.out.println("!");
                }
            } else {
                //System.out.printf("i++; i = %d\n", i);
                i++;
            }
            //}
        }
        
        i = 0;
        while (i < fuelModel.getRowCount()) {
            double currentFuel = (double) fuelTable.getValueAt(i, 3);
            //System.out.printf("Proceeding %d of %d = %f\n", i, fuelModel.getRowCount(), currentFuel);
            if ((Math.abs(currentFuel) < fuelVal) || (((currentFuel < 0) && (!showMinus)) || ((currentFuel > 0) && (!showPlus)))) {
                fuelModel.removeRow(i);
                try {
                    for (Integer key : fuelTableMap.keySet()) {
                        if (key >= i) {
                            fuelTableMap.put(key, fuelTableMap.get(key + 1));
                        }
                    }
                } catch (ConcurrentModificationException e) {
                    System.out.println("!");
                }
            } else {
                //System.out.printf("i++; i = %d\n", i);
                i++;
            }
            //}
        }
        
        /*int j = fuelModel.getRowCount() - 1;
        if (j >= 0) {
            double currentFuel = (double) fuelTable.getValueAt(j, 3);
            if ((Math.abs(currentFuel) < 5) || (currentFuel < 0 && (!showMinus)) || (currentFuel > 0 && (!showPlus))) {
                fuelModel.removeRow(j);
                try {
                    for (Integer key : fuelTableMap.keySet()) {
                        if (key >= j) {
                            fuelTableMap.put(key, fuelTableMap.get(key + 1));
                        }
                    }
                } catch (ConcurrentModificationException e) {
                    System.out.println("!");
                }
            }
        }*/
        FuelPanel.fuelLabel.setText("<html><font color=\"red\">Изменения уровня топлива (>" + Settings.GetFuelChangeVal() + " л)."
                + " Количество: " + fuelTable.getRowCount() + "</font>");
        fuelTable.updateUI();
    }

    private static void correctCntTables() {
        DefaultTableModel cnt1Model = (DefaultTableModel) cnt1Table.getModel();
        if (cnt1Model.getRowCount() >= 2) {
            String str1 = (String) cnt1Table.getValueAt(cnt1Model.getRowCount() - 1, 0);
            String str2 = (String) cnt1Table.getValueAt(cnt1Model.getRowCount() - 2, 0);
            if (str1.equals(str2)) {
                cnt1Model.setRowCount(cnt1Model.getRowCount() - 1);
            }
        }

        DefaultTableModel cnt2Model = (DefaultTableModel) cnt2Table.getModel();
        if (cnt2Model.getRowCount() >= 2) {
            String str1 = (String) cnt2Table.getValueAt(cnt2Model.getRowCount() - 1, 0);
            String str2 = (String) cnt2Table.getValueAt(cnt2Model.getRowCount() - 2, 0);
            if (str1.equals(str2)) {
                cnt2Model.setRowCount(cnt2Model.getRowCount() - 1);
            }
        }
    }
}
